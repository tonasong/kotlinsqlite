package com.example.sqlite

import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import android.graphics.Color
import android.widget.Toast

import com.example.sqlite.Model.BookModel


class DBHelper :SQLiteOpenHelper{

    val context:Context
    var lists = ArrayList<BookModel>()

    companion object{
        var DBNAME:String = "dbBook"
        var DBVERSION:Int = 1
        var TABLE = "tbBook"
        var CODE = "code"
        var TITLE = "title"
        var AUTHOR = "author"
        var PRICE = "price"
        var PHOTO = "photo"

    }

    constructor( context: Context):super(context,DBNAME,null,DBVERSION){
        this.context = context
    }

    override fun onCreate(db: SQLiteDatabase?) {
        var sql  = "create table IF NOT EXISTS " + TABLE + "(" +
                CODE + " INTEGER PRIMARY KEY, " +
                TITLE + " TEXT, " +
                AUTHOR + " TEXT, " +
                PRICE + " NUMERIC, " +
                PHOTO + " BLOB);"
        db?.execSQL(sql)
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        db!!.execSQL("DROP TABLE IF EXISTS $TABLE")
        onCreate(db)
    }

    //insert
    fun bookInsert(b:BookModel){
        var write  = this.writableDatabase
        var values  = ContentValues()
        values.put(CODE,b.code)
        values.put(TITLE,b.title)
        values.put(AUTHOR,b.author)
        values.put(PRICE,b.price)
        values.put(PHOTO,b.photo)

        write.insert(TABLE,null,values)
        write.close()
       Toast.makeText(context,"Add 1 record ", Toast.LENGTH_LONG).show()

    }

    //view
    fun bookView(sql:String):ArrayList<BookModel>{
        var database:SQLiteDatabase = readableDatabase
        var cursor =  database.rawQuery(sql,null)
        while (cursor.moveToNext()){
            var code:Int = cursor.getInt(0)
            var title:String = cursor.getString(1)
            var author:String = cursor.getString(2)
            var price:Double = cursor.getDouble(3)
            var byteArray:ByteArray = cursor.getBlob(4)
            lists.add(BookModel(code,title,author,price,byteArray))
        }
        return lists

    }
    fun getViewBookInformation(sql:String):Cursor{
        var database:SQLiteDatabase = readableDatabase
        return database.rawQuery(sql,null)
    }

    //get max code from sqlite
    fun getMaxCode(sql:String): Int? {
        var maxCode:Int =0
        var read  = this.readableDatabase
        var c = read.rawQuery(sql,null)
        if(c.moveToLast())
            maxCode = c.getInt(0)
        return maxCode
    }

}