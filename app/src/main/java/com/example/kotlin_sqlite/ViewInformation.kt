package com.example.kotlin_sqlite

import android.app.ProgressDialog
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.view.MenuItem
import android.widget.EditText
import android.widget.ProgressBar
import android.widget.Toast
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.kotlin_sqlite.Adapter.ViewAdapter
import com.example.sqlite.DBHelper
import com.example.sqlite.Model.BookModel
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_view.*

class ViewInformation : AppCompatActivity(){

    private var lists  = ArrayList<BookModel>()

    override fun onCreate(savedInstanceState: Bundle?){
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_view)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        var dbHelper = DBHelper(this)
        var linearLayoutManager = LinearLayoutManager(this)
            linearLayoutManager.orientation = LinearLayoutManager.VERTICAL
            recyclerView1.layoutManager = linearLayoutManager
        recyclerView1.itemAnimator = DefaultItemAnimator()

//        //get all data from sqlite
        var cursor = dbHelper.getViewBookInformation("SELECT * FROM tbBook")
        while (cursor.moveToNext()){
            var code:Int = cursor.getInt(0)
            var title:String = cursor.getString(1)
            var author:String = cursor.getString(2)
            var price:Double = cursor.getDouble(3)
            var byteArray:ByteArray = cursor.getBlob(4)
            lists.add(BookModel(code,title,author,price,byteArray))

        }
        //progressDialog.dismiss()
         var adapter = ViewAdapter(this,lists)
        recyclerView1.adapter = adapter


    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item.itemId){
            android.R.id.home->finish()
        }
        return true
    }

    fun getAdapterPosition(adapterPosition: Int) {
        Toast.makeText(applicationContext,"Click this location $adapterPosition",Toast.LENGTH_SHORT).show()
        var intent = Intent(applicationContext,DetailInformation::class.java)
        if(!(adapterPosition == 4 || adapterPosition == 3 || adapterPosition == 2 || adapterPosition == 1 || adapterPosition == 0 ))
        {
            var book = lists[adapterPosition]
            intent.putExtra("title",book.title)
        intent.putExtra("image",book.photo)
            startActivity(intent)
        }
    }


}
